# Open GL 2D game engine for windows
### Author
- Jesse Tremblay

### dependencies
- GLEW
- GLFW
- glm
- cimg
---
### compile
- build.bat

### compile dependencies
- CMake
- mingw make
- mingw64
---
### Credits
- openGL class abstractions inspired by [The Cherno](https://www.youtube.com/watch?v=W3gAzLwfIP0&list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2)
