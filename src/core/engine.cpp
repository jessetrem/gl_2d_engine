#include <iostream>
#include "./engine.h"
#include "./glfw_display.h"
#include "./game.h"
// build with config object loaded from file
Engine::Engine(Game* g): m_game(g), m_core_renderer(1000), m_cam({0, 0, 0}, 0, 0, 16 / 9, 1.57, nullptr) {}

Engine::~Engine() {}

void Engine::init() {
  // consider heap allocating display
  m_display = new GLFWDisplay(1920, 1080, "Engine v0.0.1");

  m_display->init();
  GLenum glewErr = glewInit();
  if (GLEW_OK != glewErr) {
    std::cout << "FAIL GLEW" << glewErr << std::endl;
    exit(-1);
  }
  m_core_renderer.init();

  // pass handle to loader and event system
  m_game->init(&m_core_renderer);
}

void Engine::run() {
  std::cout << "running" << std::endl;
  // start the game
  m_game->start();
  // control frame rate?
  while(m_running) {
    // pass time elapsed since last frame and manage frame pacing
    m_game->update(); // pass elapsed time
    glClearColor(0.33, 0.58, 0.93, 1); // cornflower blue
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    m_game->draw(); // pass rendering system
    m_core_renderer.render(&m_cam);
    m_display->update();
    // get batch renderer to draw the frame
    m_running = !glfwWindowShouldClose(m_display->getWindow()) && m_game->isRunning();
  }
}
