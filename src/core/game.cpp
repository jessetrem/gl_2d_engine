#include <string.h>
#include "./game.h"
#include "../ecs/base_component.h"

Game::Game(): m_running(false), m_alloc(6), m_handles(5) {} // m_alloc can hold 6 vertex data
Game::~Game() {}

// create relevant resources from provided game engine resources
void Game::init(BatchRenderer* renderer) {
  m_batch_renderer = renderer;
}

// create relevant entities
void Game::start() {
  // test allocator
  int* handles[6];

  float x = 10;
  Handle<float>* handle = m_handles.createHandle(&x);
  std::cout << "Yolo " << *(handle->m_data) << std::endl;
  delete handle;
  
  FixedSizeAllocator<int> testAloc(10);
  handles[0] = testAloc.getBlock();
  *handles[0] = 25;
  handles[1] = testAloc.getBlock();
  *handles[1] = 20;
  handles[2] = testAloc.getBlock();
  *handles[2] = 15;
  handles[3] = testAloc.getBlock();
  *handles[3] = 10;
  testAloc.debug();
  testAloc.freeBlock(handles[1]);

  testAloc.debug();
  handles[4] = testAloc.getBlock();
  *handles[4] = 20;
  testAloc.debug();
  testAloc.freeBlocks(handles[0], 2);
  testAloc.debug();
  handles[5] = testAloc.getBlock();
  *handles[5] = 30;
  testAloc.debug();
  
  VertexData square[] {
    {{-1.0f, -1.0f, -10.0f}}, 
    {{1.0f, -1.0f, -10.0f}}, 
    {{1.0f,  1.0f, -10.0f}}
  };
  VertexData square2[] {
    {{-1.0f, -1.0f, -20.0f}}, 
    {{1.0f, -1.0f, -20.0f}}, 
    {{0.0f,  1.0f, -20.0f}}
  };

  shape1 = m_alloc.getBlocks(3);
  shape2 = m_alloc.getBlocks(3);

  memcpy(shape1, &square, sizeof(square));
  memcpy(shape2, &square2, sizeof(square2));

  BananaBread bbread;
  bbread.x = 10;
  bbread.y = 10;

  BananaBread* bb = (BananaBread*)BaseComponent::getCreateFunction(BananaBread::ID)(nullptr, &bbread);

  std::cout << bb->x << " " << bb->y << " BANANA RREAD XY" << std::endl;

  m_running = true;
}
void Game::update() {}
void Game::draw() {
  m_batch_renderer->draw(shape1, 3);
  m_batch_renderer->draw(shape2, 3);
}
bool Game::isRunning() {
  return m_running;
}
