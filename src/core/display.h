#pragma once

class Display {
public:
  Display(unsigned int width, unsigned int height);
  virtual ~Display() = 0;
  virtual void init() = 0;
  virtual void makeActive() = 0;
  virtual void update() = 0;
protected:
  unsigned int m_width;
  unsigned int m_height;
};
