#pragma once

#include "../render/batch_renderer.h"
#include "../memory/fixed_size_allocator.h"
#include "../memory/handle.h"

class Game {
public:
  Game();
  ~Game();
  void init(BatchRenderer* renderer);
  void start();
  // instead of draw and update rely on a single ECS that is looped
  void update();
  void draw();
  bool isRunning();
private:
  bool m_running;
  BatchRenderer* m_batch_renderer; // replace with specialized renderer that uses this renderer
  
  VertexData* shape1;
  VertexData* shape2;
  FixedSizeAllocator<VertexData> m_alloc;
  HandleManager<float> m_handles;
  unsigned int* shape1_size;
  unsigned int* shape2_size;
};
