#include <iostream>
#include "./glfw_display.h"

GLFWDisplay::GLFWDisplay(unsigned int width, unsigned int height, std::string name) : Display(width, height), m_win_name(name) {}

GLFWDisplay::~GLFWDisplay() {}

void GLFWDisplay::init()
{
  glfwInit();
  m_win = glfwCreateWindow(m_width, m_height, m_win_name.c_str(), NULL, NULL);
  if (!m_win)
  {
    std::cout << "GLFW failed to create window" << std::endl;
    glfwTerminate();
    exit(-1);
  }
  glfwMakeContextCurrent(m_win);
  glfwSwapInterval(1);
}

void GLFWDisplay::makeActive()
{
  glfwMakeContextCurrent(m_win);
}

void GLFWDisplay::update() {
  glfwSwapBuffers(m_win);

  /* Poll for and process events */
  glfwPollEvents();
}
