#pragma once

#include <GLFW/glfw3.h>
#include <string>
#include "./display.h"

class GLFWDisplay : public Display {
public:
  GLFWDisplay(unsigned int width, unsigned int height, std::string name);
  virtual ~GLFWDisplay();
  virtual void init();
  virtual void makeActive();
  virtual void update();

  inline GLFWwindow* getWindow() { return m_win; }
private:
  GLFWwindow* m_win;
  std::string m_win_name;
};
