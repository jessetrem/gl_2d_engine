#pragma once

#include <GLEW/glew.h>
#include <glm/glm.hpp>
#include "../render/batch_renderer.h"
#include "../render/camera.h"

class GLFWDisplay; // hard coded to glfwDisplay for now
class Game;

class Engine {
public:
  Engine(Game* g);
  ~Engine();
  void init();
  void run();
private:
  bool m_running;
  GLFWDisplay* m_display;
  Game* m_game;
  BatchRenderer m_core_renderer;
  Camera m_cam; // find a better way to deal with camera
};
