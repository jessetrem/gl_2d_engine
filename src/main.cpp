#include <GLEW/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <iostream>
#include "./core/engine.h"
#include "./core/game.h"

int main();
GLFWwindow *init();
void run(GLFWwindow *win);

int main()
{
  Game game;
  Engine engine(&game);
  engine.init();
  engine.run();
}