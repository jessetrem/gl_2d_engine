#include "./base_component.h"

class ECSSystem {
public:
  ECSSystem(BaseComponent** componentList, unsigned int* componentListSizes);
  ~ECSSystem();
  void execute(unsigned int delta); // make pure virtual
private:
  BaseComponent** m_component_lists;
  unsigned int* m_component_list_sizes;
}