#pragma once
#include <cstddef>
#include <vector>
#include <tuple>
#include <string.h>
#include "../memory/fixed_size_allocator.h"
struct BaseComponent;

typedef void* EntityHandle;
typedef void* (*ComponentCreateFunction)(EntityHandle eh, BaseComponent *comp);
typedef void (*ComponentFreeFunction)(BaseComponent *comp);

struct BaseComponent {
public:
  static unsigned int registerComponent(ComponentCreateFunction create, ComponentFreeFunction free, size_t size);
  EntityHandle owner;
  // UTILITY GETTERS
  static inline ComponentCreateFunction getCreateFunction(unsigned int id) {
    return std::get<0>(m_registered_component_types[id]);
  }
  static inline ComponentFreeFunction getFreeFunction(unsigned int id) {
    return std::get<1>(m_registered_component_types[id]);
  }
  static inline unsigned int getSize(unsigned int id) {
    return std::get<2>(m_registered_component_types[id]);
  }
private:
  static std::vector<std::tuple<ComponentCreateFunction, ComponentFreeFunction, unsigned int>> m_registered_component_types;
};

template<typename Component>
void* componentCreate(EntityHandle eh, BaseComponent* comp) {
  Component* castComponent = (Component*)comp;
  Component* component = castComponent->allocator.getBlock();
  memcpy(component, comp, sizeof(Component));
  component->owner = eh;
  return (void*) component;
}

template<typename Component>
void componentFree(BaseComponent *comp) {
  Component* component = (Component*)comp;
  component->allocator.freeBlock(component);
}

template<typename T, unsigned int COUNT>
struct Component : public BaseComponent {
  static const ComponentCreateFunction CREATE;
  static const ComponentFreeFunction FREE;
  static const unsigned int ID;
  static const size_t SIZE;
  static FixedSizeAllocator<T> allocator; // TODO find a clean way to bind an allocator to a component type
};

template<typename T, unsigned int COUNT>
const unsigned int Component<T, COUNT>::ID(BaseComponent::registerComponent(componentCreate<T>, componentFree<T>, sizeof(T)));

template<typename T, unsigned int COUNT>
const size_t Component<T, COUNT>::SIZE(sizeof(T));

template<typename T, unsigned int COUNT>
const ComponentCreateFunction Component<T, COUNT>::CREATE(componentCreate<T>);

template<typename T, unsigned int COUNT>
const ComponentFreeFunction Component<T, COUNT>::FREE(componentFree<T>);

template<typename T, unsigned int COUNT>
FixedSizeAllocator<T> Component<T, COUNT>::allocator(COUNT);

// test

struct BananaBread : public Component<BananaBread, 1000> {
  float x;
  float y;
};
