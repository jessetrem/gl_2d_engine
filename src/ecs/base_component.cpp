#include "./base_component.h"

std::vector<std::tuple<ComponentCreateFunction, ComponentFreeFunction, unsigned int>> BaseComponent::m_registered_component_types;

unsigned int BaseComponent::registerComponent(ComponentCreateFunction createFn, ComponentFreeFunction freeFn, size_t size) {
  unsigned int id = m_registered_component_types.size();
  m_registered_component_types.push_back(std::tuple<ComponentCreateFunction, ComponentFreeFunction, unsigned int>(createFn, freeFn, size));

  return id;
}
