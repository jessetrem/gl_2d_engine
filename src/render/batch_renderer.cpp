#include <iostream>
#include <GLEW/glew.h>
#include "./batch_renderer.h"
#include "./camera.h"
#include "./vertex_array.h"
#include "./vertex_buffer.h"
#include "./vertex_buffer_layout.h"
#include "./shader.h"

BatchRenderer::BatchRenderer(unsigned int maxBufferSize) 
  : m_vertex_buffer_size(maxBufferSize), m_buffer_index(0), m_vertex_buffer(NULL), m_vbo(NULL), m_vao(NULL), m_sh(NULL) {}

BatchRenderer::~BatchRenderer() {
  if (m_vertex_buffer) {
    delete[] m_vertex_buffer;
  }
  if (m_vao) {
    delete m_vao;
  }
  if (m_vbo) {
    delete m_vbo;
  }
  if (m_sh) {
    delete m_sh;
  }
}

void BatchRenderer::init() {
  m_vertex_buffer = new VertexData[m_vertex_buffer_size];
  BufferLayout bl(1);
  bl.pushFloat(3);
  m_vbo = new VertexBuffer();
  m_vao = new VertexArray();
  m_vao->setData(*m_vbo, bl);
  m_sh = new Shader("./res/shaders/core.shader");
}

void BatchRenderer::draw(VertexData* verts, unsigned int vertCount) {
  if (m_buffer_index + vertCount >= m_vertex_buffer_size) {
    // out of space
    std::cerr << "No room in batch buffer" << std::endl;
    return;
  }
  for (unsigned int i = 0; i < vertCount; ++i) {
    m_vertex_buffer[m_buffer_index++] = verts[i];
  }
}

void BatchRenderer::render(Camera* cam) {
  unsigned int elementCount = m_buffer_index;
  glm::mat4x4 cameraTransform = cam->getMVP();
  m_sh->setUniformFm("camera", cameraTransform);
  m_sh->bind();
  m_vbo->setData(m_vertex_buffer, sizeof(VertexData) * elementCount, false);
  m_vao->bind();
  glDrawArrays(GL_TRIANGLES, 0, elementCount);
  reset();
}

void BatchRenderer::reset() {
  m_buffer_index = 0;
}
