#include "./shader.h"
class VertexArray;
class IndexBuffer;
class Shader;
class Camera;

class PointLight;


class Renderer
{
public:
  Renderer(PointLight *pl);
  ~Renderer();
  void init();
  void draw(VertexArray *vao, Shader *shader, IndexBuffer *index);
private:
  PointLight *pl;
  Shader m_shadow_shader;
  unsigned int m_shadow_buffer;
  unsigned int m_db;
  unsigned int m_shadow_map;
};
