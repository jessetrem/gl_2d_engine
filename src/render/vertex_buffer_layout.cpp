#include <GLEW/glew.h>
#include "./vertex_buffer_layout.h"
#include <iostream>

BufferLayout::BufferLayout(unsigned int size): size(size), stride(0), push_pos(0) {
  elements = new AttributePointer[size];
  buffer_layout_size = size;
}

BufferLayout::~BufferLayout() {
  delete [] elements;
}

void BufferLayout::pushFloat(unsigned int count) {
  if (push_pos < buffer_layout_size) {
    std::cout << "COUNT: " << count << std::endl;
    elements[push_pos] = { sizeof(GL_FLOAT), count, GL_FLOAT, false };
    stride += elements[push_pos].size * elements[push_pos].count;
    ++push_pos;
  } else {
    throw "Buffer layout full";
  }
}

AttributePointer & BufferLayout::operator[](unsigned int index) {
  return elements[index];
}

unsigned int BufferLayout::getStride() {
  return stride;
}

unsigned int BufferLayout::getSize() {
  return size;
}
