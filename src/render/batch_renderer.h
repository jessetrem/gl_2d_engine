#pragma once

#include "glm/glm.hpp"

class Camera;
class VertexArray;
class VertexBuffer;
class Shader;

struct VertexData {
  glm::vec3 pos;
};

class BatchRenderer {
public:
  BatchRenderer(unsigned int maxBufferSize);
  ~BatchRenderer();
  void init();
  void draw(VertexData* verts, unsigned int vertCount); // add texture to draw call
  void render(Camera* cam);
private:
  VertexData* m_vertex_buffer;
  unsigned int m_vertex_buffer_size;
  unsigned int m_buffer_index;
  VertexBuffer* m_vbo;
  VertexArray* m_vao;
  Shader* m_sh;
  
  void reset();
};
