#pragma once
/**
 * @brief Object oriented abstraction of an opengl Element Array Object
 * 
 */
class IndexBuffer {
  public:
    IndexBuffer();
    ~IndexBuffer();
    unsigned int getSize();
    void setData(unsigned int *data, unsigned int size);
    void bind();
    void unbind();
  private:
    /**
     * @brief Bind a buffer without overwriting the current_buffer
     * 
     */
    void stateless_bind();
    unsigned int ib;
    unsigned int m_size;
};