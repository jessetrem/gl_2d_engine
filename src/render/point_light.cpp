#include <iostream>
#include "./point_light.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

PointLight::PointLight(glm::vec3 position, glm::vec3 color, float intensity) : m_position(position), m_color(color), m_intensity(intensity), m_velocity({0.0f, 0.0f, 0.0f}), m_direction(0.0f, 0.0f, 0.0f), m_bias(0.02f) {}

PointLight::~PointLight() {}

glm::vec3 & PointLight::getPosition() {
  return m_position;
}

glm::vec3 & PointLight::getColor() {
  return m_color;
}

float & PointLight::getIntensity() {
  return m_intensity;
}

glm::mat4x4 PointLight::getMVP() {
  // std::cout << m_position.x << " " << m_position.y << " " << m_position.z << std::endl;
  return glm::perspective(glm::radians(90.0f), 1.0f, 0.1f, m_intensity) * glm::lookAt(m_position, m_direction, glm::vec3(0,1,0));
}

void PointLight::update() {
  m_position += m_velocity;
}

void PointLight::applyForce(glm::vec3 force) {
  m_velocity += force;
}

void PointLight::modifyBias(float amount) {
  m_bias += amount;
}

float & PointLight::getBias() {
  return m_bias;
}

MoveLightCommand::MoveLightCommand(PointLight *pl, glm::vec3 force): m_force(force), m_point_light(pl) {}
MoveLightCommand::~MoveLightCommand() {}

void MoveLightCommand::execute() {
  m_point_light->applyForce(m_force);
}

ChangeLightBiasCommand::ChangeLightBiasCommand(PointLight *pl, float amount): m_amount(amount), m_point_light(pl) {}
ChangeLightBiasCommand::~ChangeLightBiasCommand() {}

void ChangeLightBiasCommand::execute() {
  m_point_light->modifyBias(m_amount);
}
