#include <GLEW/glew.h>
#include <GLFW/glfw3.h>
#include "./renderer.h"
#include "./vertex_array.h"
#include "./index_buffer.h"
#include "./shader.h"
#include "./point_light.h"

glm::vec3 lightPos(0.0f, 25.0f, 0.0f);

Renderer::Renderer(PointLight *pl): pl(pl), m_shadow_shader("./res/shaders/shadow.shader") {
  init();
}
Renderer::~Renderer() {
}

void Renderer::init() {
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  glFrontFace(GL_CCW);
  glCullFace(GL_BACK);
  glPointSize(10.0f);
  glLineWidth(10.0f);
  
  glGenFramebuffers(1, &m_shadow_buffer);
  glGenTextures(1, &m_shadow_map);
  glGenRenderbuffers(1, &m_db);
}

// TURBO YIKES 4head
void Renderer::draw(VertexArray *vao, Shader *shader, IndexBuffer *index) {
  // setup depth texture
  unsigned int shadowRes = 1000;
  glBindFramebuffer(GL_FRAMEBUFFER, m_shadow_buffer);
  glBindFramebuffer(GL_FRAMEBUFFER, m_shadow_buffer);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, m_shadow_map);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, shadowRes, shadowRes, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glBindRenderbuffer(GL_RENDERBUFFER, m_db);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, shadowRes, shadowRes);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_db);
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_shadow_map, 0);
  GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
  glDrawBuffers(1, DrawBuffers);
  glBindFramebuffer(GL_FRAMEBUFFER, m_shadow_buffer);
  glViewport(0,0,shadowRes, shadowRes);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    return;
  // shadow pass
  glm::mat4x4 lightMatrix = pl->getMVP();
  m_shadow_shader.setUniformFm("cam", lightMatrix);
  m_shadow_shader.setUniformF("range", pl->getIntensity());
  m_shadow_shader.bind();
  vao->bind();
  index->bind();
  glDrawElements(GL_TRIANGLES, index->getSize(), GL_UNSIGNED_INT, (void*) 0);
  // color pass
  glm::mat4 correctionMatrix(
      0.5, 0.0, 0.0, 0.0,
      0.0, 0.5, 0.0, 0.0,
      0.0, 0.0, 0.5, 0.0,
      0.5, 0.5, 0.5, 1.0);
  glm::mat4x4 shadowBias = correctionMatrix * lightMatrix;

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glViewport(0, 0, 1920, 1080);
  float bias = 0.035f;
  shader->setUniform1i("depth_map", 0); // bind shadow sampler to texture 0
  shader->setUniformF("light.intensity", pl->getIntensity());
  shader->setUniformF("bias", pl->getBias());
  shader->setUniformF("range", pl->getIntensity());
  shader->setUniformFv("light.color", pl->getColor());
  shader->setUniformFv("light.position", pl->getPosition());
  shader->setUniformFm("light_view", shadowBias);
  shader->bind();
  glDrawElements(GL_TRIANGLES, index->getSize(), GL_UNSIGNED_INT, (void*) 0);
}
