#include "./window_commands.h"

CloseWindowCommand::CloseWindowCommand(GLFWwindow *window): window(window) {}
CloseWindowCommand::~CloseWindowCommand() {}

void CloseWindowCommand::execute()
{
    glfwSetWindowShouldClose(window, true);
}
