#pragma once
#include <GLFW/glfw3.h>
#include "./command.h"

/**
 * @brief command that closes the glfw window
 * 
 */
class CloseWindowCommand : public Command {
public:
  CloseWindowCommand(GLFWwindow *window);
  ~CloseWindowCommand();
  void execute();
private:
  GLFWwindow *window;
};