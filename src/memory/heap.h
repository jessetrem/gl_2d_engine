#include <functional>

template <typename T>
struct HeapElement {
  T* m_element;
  HeapElement<T>* m_left;
  HeapElement<T>* m_right;
};

// use sorting function as lambda
template <typename T>
class Heap {
public:
  Heap(std::function<bool(T, T)> comparator);
  ~Heap();
  void push(T item);
  T pop();
  T peek();
private:
  HeapElement<T> m_root;
  std::function<bool(T, T)> m_comp;
};
