#pragma once
#include <stack>
#include <iostream>
// TODO memory allignment, dealing with memory limits
// TODO reimplement using handles and treat as component store rather than some generic allocator
// template<typename P>
// struct Handle {
//   P* data;
// };

template<typename T>
class FixedSizeAllocator {
public:
  FixedSizeAllocator(unsigned int count) {
    m_memory = new T[count];
    m_memory_size = count;
    m_tail = &(m_memory[0]);
  }

  ~FixedSizeAllocator() {
    delete[] m_memory;
  }
  
  T* getBlock() {
    T* freeStart = m_tail++;
    
    return new(freeStart) T;
  }

  T* getBlocks(unsigned int count) {
    T* freeStart = m_tail;
    m_tail += count;
    return new(freeStart) T[count];
  }

  void freeBlock(T* blockHandle) {
    --m_tail;
    int squashStartIndex = blockHandle - m_memory;
    int lastIndex = m_tail - m_memory;
    for (int i = squashStartIndex; i < lastIndex; ++i) {
      m_memory[i] = m_memory[i + 1];
    }
  }

  void freeBlocks(T* start, unsigned int count) {
    m_tail -= count;
    int squashStartIndex = start - m_memory;
    int lastIndex = m_tail - m_memory;
    for (int i = squashStartIndex; i < lastIndex; ++i) {
      m_memory[i] = m_memory[i + count];
    }
  }

  void debug() {
    std::cout << "Allocator size: " << m_memory_size
      << "\nElement Size: " << sizeof(T)
      << "\n==========================================\nMEMORY\n==========================================" << std::endl;
    for (int i = 0; i < m_memory_size; ++i) {
      std::cout << (int)m_memory[i] << " "; 
    }
    std::cout << std::endl;
  }

private:
  T* m_memory; // byte array
  // Handle<T*> m_handles;
  unsigned int m_memory_size; // number of allocatable elements
  T* m_tail; // currently only does m_tail allocation  
};
