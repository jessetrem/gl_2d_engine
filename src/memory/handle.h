#include <stack>
#include <stdlib.h>
#include <iostream>

template <typename T>
class HandleManager;

template <typename T>
struct Handle
{
  Handle(HandleManager<T>* owner, T* data): m_owner(owner), m_data(data) {}

  ~Handle()
  {
    std::cout << "do not call me" << std::endl;
  }

  HandleManager<T>* m_owner;
  T* m_data;
};

template <typename T>
class HandleManager
{
public:
  HandleManager(unsigned int size) : m_store_size(size)
  {
    m_handle_store = (Handle<T> *)malloc(sizeof(Handle<T>) * size);
  }

  ~HandleManager()
  {
    free(m_handle_store);
  }

  Handle<T>* createHandle(T* data)
  {
    Handle<T>* h;
    if (m_free_stack.empty())
    {
      h = m_handle_store + m_handle_index;
      ++m_handle_index;
    }
    else
    {
      h = m_free_stack.top();
      m_free_stack.pop();
    }

    return new(h) Handle<T>(this, data);
  }

  void deleteHandle(Handle<T>* target) {
    m_free_stack.push(target);
  }

private:
  Handle<T> *m_handle_store;
  unsigned int m_store_size;
  unsigned int m_handle_index;

  std::stack<Handle<T>*> m_free_stack;
};