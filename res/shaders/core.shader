#version 400 core

layout (location = 0) in vec3 vert;

uniform mat4 camera;
// uniform mat4 world;

uniform float range;

out float v_color;

void main() {
  vec4 newPos = camera * vec4(vert, 1.0);
  gl_Position = newPos;
  v_color = 0;
}

~~pixel_boi~~
#version 400 core

in float v_color;

layout(location = 0) out vec3 color;

void main() {
  color = vec3(1,0,0);
}